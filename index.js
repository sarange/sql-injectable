const express = require("express");
const bodyParser = require('body-parser');
const sqlite3 = require('sqlite3').verbose();
const app = express();
const jsonParser = bodyParser.json()
const port = 1337;

let db = create_db();

app.set("view engine", "ejs");

app.get("/", (req, res) => {

    res.render("page");
})

app.post("/login", jsonParser, (req, res) => {
    login(db, req.body, res, req);
})

app.use('/static', express.static('static'))

app.use((req, res) => {
    res.status(404).send("<h1>Page not found on the server</h1>")
})

app.listen(port, () => {
    console.log(`App started on port ${port}`)
})


function create_db() {
    let db = new sqlite3.Database(':memory:');
    db.serialize(function() {
        let username = (Math.random() + 1).toString(36).substring(2);
        let password = (Math.random() + 1).toString(36).substring(2);
        db.run('CREATE TABLE users(username text, password text);');
        db.run("INSERT INTO users VALUES(?, ?);", [username, password]);
    });
    return db
}


function login(db, creds, res, req) {
    let sql = `select * from users where username='${creds['username']}' and password='${creds['password']}';`
    db.get(sql, [], (err, row) => {
        if (typeof row !== 'undefined'){
            console.log(`Successful audit | Login | username=${creds['username']} | IP=${req.socket.remoteAddress }`)
            res.status(200).send(`Logged in as ${creds['username']}`);  
        } else {
            console.log(`Failure audit | Login | username=${creds['username']} | IP=${req.socket.remoteAddress }`)
            res.status(200).send('Wrong username or password');
        }
    });
}
